#@field PORTNAME
#@type STRING
#The port name. Should be unique within an IOC.
#
#@field IPADDR
#@type STRING
#IP or hostname of the TCP endpoint.
#
#@field PREFIX
#@type STRING
#Prefix for EPICS PVs.

epicsEnvSet(IPPORT, "4001")

require pvaSrv,0+
require recsync,1.0+

 
#Specify the TCP endpoint and give your 'bus' an arbitrary name eg. "kjlc392stream".
drvAsynIPPortConfigure("kjlc392stream", "$(IPADDR):$(IPPORT)")
#drvAsynIPPortConfigure("kjlc392stream", "localhost:4001")

#asynSetTraceMask("asynstream1", -1)
#asynSetTraceIOMask("asynstream1", -1)
 
#Load your database defining the EPICS records
dbLoadRecords(kjlc392.template, "PREFIX=$(PREFIX), ADDR=1, PROTOCOL=kjlc392.proto, PORT=kjlc392stream")
